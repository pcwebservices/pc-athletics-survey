<?php
/*
* Plugin Name: PC Survey App
* Plugin URI: http://www.providence.edu
* Description: Allows you to add a survey for students
* Version: 1.0
* Author: Matt Bevilacqua
* Author URI: http://www.providence.edu
* License: GPL2
*/

/* REGISTER NEW POST TYPES */
require_once('core/includes/pc-survey-post-types.php');

/* HANDLE DISPLAY OF SUBMISSION ON THE ADMIN INTERFACE */
require_once('core/includes/pc-survey-metabox.php');

/* ACF INCLUDES */
require_once('core/includes/pc-survey-acf.php');

/* HANDLE EMAIL NOTIFICATION FOR THE COMPLETED QUIZ*/
require_once('core/includes/pc-survey-dispatch-notification.php');

/* functions to get array for rest */
require_once('core/includes/pc-survey-get-questions.php');
require_once('core/includes/pc-survey-get-results-from-id.php');

/* Set up the rest api routes for the survey*/
require_once('core/includes/pc-survey-rest-routes.php');

/* PC Survey Options */
require_once('core/includes/pc-survey-options.php');

/* REGISTER SCRIPTS & STYLES */

function pcsurvey_styles() {

	if( is_singular( 'survey' ) ){
     
    	wp_enqueue_style( 'pcsurveystyles', plugins_url('core/css/pcsurvey.css', __FILE__ ), array(), null );
    	wp_enqueue_script( 'pcsurveysscripts', plugins_url('core/js/single-survey.js', __FILE__ ), array('jquery', 'vue'), null);
      wp_enqueue_script( 'vue', plugins_url('core/js/vue.js', __FILE__), array('jquery'), null );
      
	}

  wp_localize_script( 'pcsurveysscripts', 'wpApiSettings', array( 
    'wpJson' => esc_url_raw( get_rest_url() ),
    'nonce' => wp_create_nonce( 'wp_rest' ) 
  ) ); 

}  

add_action('wp_enqueue_scripts', 'pcsurvey_styles');

/* REGISTER ADMIN SCRIPTS & STYLES */

add_action( 'admin_enqueue_scripts', 'pcsurvey_admin_scripts' );

function pcsurvey_admin_scripts(){

  wp_enqueue_script( 'vue', plugins_url('core/js/vue.js', __FILE__), array('jquery'), null );
  wp_enqueue_script('pcsurveycharts', plugins_url('core/js/chart.min.js', __FILE__), array(), '2.60' );

  wp_enqueue_script('pcsurveyd3admin', plugins_url('core/js/admin.js', __FILE__ ), array('pcsurveycharts', 'vue'), '0.1' );
  wp_localize_script( 'pcsurveyd3admin', 'restURL', array( 'wpJson' => get_rest_url() ) ); 

}

require_once('core/includes/pc-survey-admin-styles.php');

/* REGISTER NEW TEMPLATES FOR QUIZ POST TYPE */
function pcsurvey_survey_post_type_template($single_template) {
  global $post;
  if ($post->post_type == 'survey'){
     $single_template = plugin_dir_path( __FILE__ ) . '/core/templates/single-survey.php';
  } else if ($post->post_type == 'submission'){
  	     $single_template = plugin_dir_path( __FILE__ ) . '/core/templates/single-submission.php';

  }
  return $single_template;
}

add_filter( 'single_template', 'pcsurvey_survey_post_type_template' );

register_activation_hook( __FILE__, 'pc_survey_import_survey' );

function pc_survey_import_survey(){
  
  if ( is_admin() ){
    
    $json = file_get_contents( __DIR__ . '/sample.json' );

    $s = json_decode( $json ); 
    $meta = array();

    foreach( $s as $t ){
      $meta[$t->key] = $t->value;
    }
   
    $args = array( 'post_title' => 'Student-Athlete Exit Interview' , 'meta_input' => $meta, 'post_status' => 'publish', 'post_type' => 'survey' );   
    $p =  wp_insert_post( $args );

    if( $p ){
      set_transient( 'pc_sample_survey', $p );
    } else {
      return;
    }

  } else {

    return;

  }

}

register_deactivation_hook( __FILE__, 'pc_survey_deactivator' );

function pc_survey_deactivator(){

  $p = get_transient('pc_sample_survey');

  if( $p ){
    wp_delete_post( $p );
    delete_transient( 'pc_sample_survey' );
  }
  
}