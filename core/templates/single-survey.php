<!DOCTYPE html>
<html class="no-js" lang="en">
	<head>
	    <meta charset="utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	    <meta name="viewport" content="width=device-width" />
	    
	    <?php wp_head(); ?>

	</head>

<body data-post-id="<?php the_id();?>">

	<?php get_header(); ?>

	<div class="wrap">
		<div id="survey">
			<form enctype="multipart/form-data" encoding="multipart/form-data" action="" id="sendResultsForm" name="sendResultsForm" method="POST">
				
				<survey-question></survey-question>
				<div v-show="surveyComplete">Thank you for taking our survey!</div>
				<answer-review v-show="showAnswersForReview"></answer-review>
				
				<input type="hidden" id="surveyResults" name="surveyResults" />
				<?php wp_nonce_field( 'pcsurvey_sendresults', 'post_nonce_field' ); ?>
				<input type="text" name="lastName" style="position: absolute; margin-left: -999999px;" />
				<span v-show="surveyComplete && !surveySubmitted" v-on:click="submitSurvey()">Submit Your Survey</span>			 				
				<!--<span v-show="surveyComplete && !surveySubmitted" v-on:click="reviewAnswers()">Review Answers</span>-->

			</form>
		</div>
	</div>

	<script type="text/x-template" id="answer-review">
		<div>
			<ul>
				<li>Total Questions: {{ totalQuestions }}</li>
				<li>Total Answers:  </li>			
			</ul>			
			<ul>
				<li v-for="(question, key) in questions" v-if="question.type != 'message' && question.type != 'page_break' && question.type != 'heading' && key != 'title'">	
				</li>
			</ul>

		</div>
	</script>

	<script type="text/x-template" id="survey-question">
		<div class="survey-page">

			<progress v-bind:value="progress" v-bind:max="totalQuestions"></progress>

			<h1>{{ questions.title }}</h1>			

			<div class="survey-question" v-for="(question, key) in questions" v-if="question.page == page" v-bind:data-key="key">
		 			
		 		<div class="fade-in message" v-if="question.type == 'message'">
		 			<p v-html="question.contents">{{ question.contents }}</p>
		 		</div>
		 
		 		<div class="fade-in page-break" v-else-if="question.type == 'page_break'">
		 			<hr>
		 		</div>

		 		<div class="fade-in heading" v-else-if="question.type == 'heading'">
		 			<h2>{{question.contents}}</h2>
		 		</div>

		 		<div class="fade-in question" v-else>
		 			
		 			<label v-bind:class="question.required">{{ question.question }}</label>
		 			
		 			<!-- if the question is a text input -->
		 			<input type="text" v-if="question.type == 'text'" v-bind:class="question.required"  v-on:click="removeErrorClass($event)" v-model="question.answer" />

		 			<!-- if the question is a multiple lines of text input -->
		 			<textarea v-if="question.type == 'textarea'" v-bind:class="question.required" v-on:click="removeErrorClass($event)" v-model="question.answer"></textarea>
		 			
		 			<!-- if the question is a yes or no question -->
		 			<select v-if="question.type == 'yes_no'" v-bind:class="question.required" v-on:click="removeErrorClass($event)" v-model="question.answer">
		 				<option disabled> --- </option>
		 				<option value="yes">Yes</option>
		 				<option value="no">No</option>
		 			</select>

		 			<select v-if="question.type =='radio'"" :class="question.required" v-on:click="removeErrorClass($event)" v-model="question.answer">
		 				<option v-bind:value="value" v-for="( value, key ) in question.answers" v-on:click="removeErrorClass($event)">{{ value }}</option>
		 			</select>

		 			<select v-if="question.type == 'multiple'"" v-bind:class="[ question.required, { multiple : 'multiple' } ]" v-on:click="removeErrorClass($event)" multiple="multiple">
		 				<option disabled> --- </option>
		 				<option v-bind:value="value" v-for="( value, key ) in question.answers" v-on:click="removeErrorClass($event)">{{ value }}</option>
		 			</select>

		 			<select v-if="question.type =='rank'" v-bind:class="question.required" v-model="question.answer">
		 				<option disabled> --- Please rank using the options below --- </option>
		 				<option value="1"> Below Average</option>
		 				<option value="2"> 2 </option>
		 				<option value="3"> 3 </option>
		 				<option value="4"> 4 </option>
		 				<option value="5"> Above Average </option>
		 			</select>					

		 		</div>	 		

		 	</div>		

		 	<span v-show="!this.$parent.surveyComplete" class="fade-in" v-on:click="nextPage()">Next Page</span>
		 	<span v-show="!this.$parent.surveyComplete && page > 0" class="fade-in" v-on:click="prevPage()">Previous Page</span>

		</div>
	</script>

	<?php get_footer(); ?>

</body>
	
</html>

