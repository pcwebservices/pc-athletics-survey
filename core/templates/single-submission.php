<?php
    

	if( !current_user_can( "edit_posts" ) ){
		exit("You are not authorized to view this page. Please login back into your site and try again.");
		
	}

	if( !isset( $_GET['auth'] )  || $_GET['auth'] != "true" ){		
		var_dump( $_GET );

		exit("You are not authorized to view this page. Please login back into your site and try again.");
	}

	if( !isset( $_GET['_wpnonce'] ) || !wp_verify_nonce(  $_GET['_wpnonce'], 'view_submission') ){
		exit("You are not authorized to view this page. Please login back into your site and try again.");
	}
 

    //creates the filename
    $filename = preg_replace('/[^a-zA-Z0-9-_\.]/','', get_the_title() ) . '.csv';

 	// output headers so that the file is downloaded rather than displayed
	header('Content-Type: text/csv; charset=utf-8');
	header('Content-Disposition: attachment; filename=' . $filename);

	// create a file pointer connected to the output stream
	$output = fopen('php://output', 'w');

	$submissionJson = get_post_meta( $post->ID, 'submissionJson', true);
	$studentName = get_post_meta( $post->ID, 'submissionName', true ); 
	$studentEmail= get_post_meta( $post->ID, 'submissionEmail', true ); 
	$studentMajor = get_post_meta( $post->ID, 'submissionMajor', true ); 
	$classYear = get_post_meta( $post->ID, 'submissionClassYear', true );
	date_default_timezone_set('EST');
	$timeStamp = date('F j Y g:i:s A e', $outputJson->timeStamp/1000 );

	$outputJson = json_decode( $submissionJson );

	fputcsv( $output, array('Student Name:', $studentName) );
	fputcsv( $output, array('Student Email:', $studentEmail) );
	fputcsv( $output, array('Student Major:', $studentMajor) );
	fputcsv( $output, array('Class Year', $classYear) );
	fputcsv( $output, array('Time of Survey', $timeStamp) );
	fputcsv( $output, array('') );

	// output the column headings
	fputcsv( $output, array('Question', 'Answered', 'Correct', 'Answer Given', 'Correct Answer')) ;

	foreach( $outputJson as $row ){
		if( $row->correct == true){
			$correct = "Yes";
		} else {
			$correct = "No";
		}

		if( $row->answered == true ){
			$answered = "Yes";
		} else {
			$answered = "No";
		}

		if( $row->questionTitle != '' ){
			fputcsv( $output, array( $row->questionTitle, $answered, $correct,  $row->answerGiven, $row->correctAnswer ) );
		}
	}

	fclose($output);