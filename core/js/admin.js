// We use window.onload so that all the inline vue templates are able to be compiled
window.onload = function() {

	//this checks for the presence of a surveyData element before running any of this special Javascript
	if( document.getElementById('surveyData') ) {		

		// This is an array of colors to use for creating a chart
		var bgColors = ["#E53935", "#039BE5", "#43A047", "#FDD835", "#F4511E", "#5E35B1", "#FF1744", "#F50057", "#D500F9", "#651FFF", "#3D5AFE" ];

		var surveyData = new Vue({
		
		 	el: '#surveyData',

		 	data: {
		 		surveys: [],
		 		questions: [],
				filters: false,
				currentPage: false, 
		 		displayQuestions: false,
		 		charts: [],
		 		selectedSurvey: false,
		 		dataFiltered: false,
				loading: false,
				maxPage: 0, 
		 	},

		 	mounted: function(){
				var self = this;
		 		jQuery.ajax({ 
					url: restURL.wpJson + 'pc-survey/v1/surveys',
						success: function( res ){
							self.surveys = res;
		   			 	}	   			 	
		 		});

		 	},

		 	methods:{

		 		visible: function( type, key, page ){		 			

		 			if( type == undefined ){
		 				return false;
		 			}
		 			
		 			if( type == 'page_break' || type == 'heading' || type == 'message' ){		 				
		 				return false;
		 			} 			

		 			if( this.filters.index == key ){
		 				return false;
					}
					 
					if( this.currentPage && this.currentPage != page ){
						return false;
					}

		 			return true;
		 		},

		 		clearAllFilters: function(){

		 			var filterSelect = document.getElementById('dataFilter');
		 			filterSelect.value = 0;
					this.dataFiltered = false;
					this.currentPage = false; 
					this.recalculateQuestions();
					


				 },

				 chartWrapperClass: function( type ){
					return 'chartWrapper ' + type;
				 },

		 		recalculateQuestions: function(){
		
		 			var self = this;
					var selectedSurvey = document.getElementById('selectSurvey').value;			 				
		 			
		 			jQuery.ajax({ 
						url: restURL.wpJson + 'pc-survey/v1/surveys?id=' + selectedSurvey,
						success: function( res ){						
							self.questions = res;
							self.displayQuestions = true;															
							var arr = Object.keys(res).map(function (key) { return res[key]; });
							self.selectedSurvey = selectedSurvey;
							self.populateCharts( arr );			
							self.setMaxPage();				
						}
					});	
					 
					

		 		},

		 		populateCharts: function( arr ){

		 			this.loading = true;
				
		 			var self = this;
		 			var answers = [];
		 			var type, dataset, labels;		 			

		 			arr.forEach( function( currentValue, index, array ) {

		 				var timeout = 1000 + index*20;

		 				setTimeout( function() {

		 				if( !currentValue.is_filter ){

			 					var url = restURL.wpJson + 'pc-survey/v1/results?id=' + self.selectedSurvey + '&question=' + index;

			 					if( parseInt(self.filters.index) && self.dataFiltered == true ) {	
			 						self.filters.value = document.getElementById('dataFilter').value;	 						
			 						url += '&filter=' + self.filters.index + '&filterValue=' + self.filters.value;
			 					} 

						 		jQuery.ajax({ 
									url: url,
									timeout: 30000, 							
									success: function( res ){	

										var el = document.getElementById(index);

										if( res == false && el != null ){
											el.innerHTML = '';
											return;	
										}


										var dataset = new Array();

										if( res[0] == undefined ){
											return false;
										}		

										var questionType = res[0].type;
										var question = res[0].question;							

										for (var i = res.length - 1; i >= 0; i--) {
											answers[i] = res[i].answer;
										}							

										if( questionType == "rank" ){
											
											//this chart is a bar chart
											type = "bar";

											// set the 5 data points to 0
											dataset[0] = dataset[1] = dataset[2] = dataset[3] = dataset[4]  = 0;
											
											// set the labels to the five options
											labels = ["Below Average","2","3","4","Above Average"];
											
											answers.forEach( function( value, index, answers) {
												if( value != ''){
													dataset[ parseInt(value)-1 ]++;
												}
											});
																		
										} else if(questionType =="yes_no"){															

											type = "pie";
											dataset[0] = 0;
											dataset[1] = 0;

											for (var i = answers.length - 1; i >= 0; i--) {
												if( answers[i] == 'yes'){
													dataset[0]++;
												} else if( answers[i] == 'no' ){
													dataset[1]++;
												} 									
											}									

											labels = [ "Yes", "No" ];
									

										} else if( questionType == "radio" ){
											
											type = "doughnut"; 
											labels = new Array();
											dataset = new Array();			
											var x = 0;
											answers = answers.sort();								

											answers.forEach( function(value, index, answers){

												if (value == ''){
													answers[index] = 'false';
												}								

												if ( answers[index] != 'false'){									
													if( dataset[x] == undefined ){
														labels.push(value);
														dataset[x] = 1;
													} else if ( answers[ index - 1] != value){										
														labels.push( value );
														x++;
														dataset[x] = 1;
													} else {													
														dataset[x]++;
													}
												}

											});				
											
										} else if( questionType == "multiple" ){

											type = "polarArea";
											labels = new Array();
											dataset = new Array();
											var data = new Array();
											var x = 0;

											answers.forEach( function( value, index, answers){
												if( value != '' ){															
													value.sort();						
													value.forEach( function( value, index){
														data.push( value );
													});
													
												}
											});

											data.sort().forEach( function( value, index, data){

												if (value == ''){
													data[index] = 'false';
												}								

												if ( data[index] != 'false'){									
													if( dataset[x] == undefined ){
														labels.push(value);
														dataset[x] = 1;
													} else if ( data[ index - 1] != value){										
														labels.push( value );
														x++;
														dataset[x] = 1;
													} else {													
														dataset[x]++;
													}
												}

											});								

										} //closes questionType if/else

										self.charts[index] = [ type, dataset, labels, question ];	

										if( questionType == "text" || questionType == "textarea"){

											self.displayListOfResults( el, answers );
											
										} else {													

											self.drawChart( el, type, dataset, labels, question);

										}// closes drawChart toggle									

									} // closes ajax success

					 			}); // closes ajax call

					 		} else { //if "isn't filter"
					 			
					 			self.filters = currentValue;
					 			self.filters.index = index;

					 		} 


							if( index == array.length-1 ){
								setTimeout( function(){
									self.loading = false;
								}, timeout*2);
							}					 		

					 	}, timeout);


			 		}); // closes iteration over array		

			 		
				 },
				 
				setCurrentPage: function( event ){
					var currentPageSelected = document.querySelector('#currentPageSelect').value;
					this.currentPage = currentPageSelected - 1;
					this.recalculateQuestions();
				},

		 		filterData: function(){		 			
		 			this.dataFiltered = true;
		 			this.recalculateQuestions();
				 },
				 
				setMaxPage: function(){
					var max = Object.keys( this.questions ).length - 3;				
					this.maxPage = this.questions[max].page + 1;
				},

				displayListOfResults: function( el, answers ){

			 		if( el == null){
		 				return;
		 			}

		 			el.innerHTML = '';	 			
					var chartWrapper = el;					

						// create a list and an empty string in which to store the list
					var list = document.createElement('ul');
					var listHTML = '';

					//loop through the answers and put them in the html string
					answers.forEach( function( value, index, answers){

						if( value != '' ){
							listHTML += '<li>' + value + '</li>';
						}

					});

					//print it out
					list.innerHTML = listHTML;
					chartWrapper.appendChild( list );

		 		}, 	

		 		drawChart: function( el, type, dataset, labels, question ){

		 			if( el == null){
		 				return;
		 			}

		 			el.innerHTML = '';

		 		//	console.log( type );

		 			var tooltipsEnabled = false;
		 			var showAllTooltips = true;

		 			if( type == 'bar' ){
		 				tooltipsEnabled = true;
		 				showAllTooltips = false;
		 			}

		 			// Show tooltips always even the stats are zero
						Chart.pluginService.register({
						  beforeRender: function(chart) {
						    if (chart.config.options.showAllTooltips) {
						      // create an array of tooltips
						      // we can't use the chart tooltip because there is only one tooltip per chart
						      chart.pluginTooltips = [];
						      chart.config.data.datasets.forEach(function(dataset, i) {
						        chart.getDatasetMeta(i).data.forEach(function(sector, j) {
						          chart.pluginTooltips.push(new Chart.Tooltip({
						            _chart: chart.chart,
						            _chartInstance: chart,
						            _data: chart.data,
						            _options: chart.options.tooltips,
						            _active: [sector]
						          }, chart));
						        });
						      });

						      // turn off normal tooltips
						      chart.options.tooltips.enabled = tooltipsEnabled;
						    }
						  },
						  afterDraw: function(chart, easing) {
						    if (chart.config.options.showAllTooltips) {
						      // we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
						      if (!chart.allTooltipsOnce) {
						        if (easing !== 1)
						          return;
						        chart.allTooltipsOnce = true;
						      }

						      // turn on tooltips
						      chart.options.tooltips.enabled = true;
						      Chart.helpers.each(chart.pluginTooltips, function(tooltip) {
						        tooltip.initialize();
						        tooltip.update();
						        // we don't actually need this since we are not animating tooltips
						        tooltip.pivot();
						        tooltip.transition(easing).draw();
						      });
						      chart.options.tooltips.enabled = tooltipsEnabled;
						    }
						  }
						});

						// Show tooltips always even the stats are zero
				

	 				// 1) whenever redraw chart is called, the container is emptied and a new canvas element is placed inside
					var chartWrapper = el;					
					var chart = document.createElement('canvas');

					chartWrapper.appendChild( chart );					

					// 2) This grabs the canvas element we are going to use to stick our chart in		
					var ctx = chart.getContext('2d');

					// 3) This passes the data to the chart and makes it appear
					var myChart = new Chart(ctx, {
						type: type,					  
						data: {
						labels: labels,
						datasets: [{
								label: question, 
								backgroundColor: bgColors,
								data: dataset,
							}]
						},
						options: {
							showAllTooltips: showAllTooltips,
						},
					});	
		 			
		 		},

		 	}	

		});	

	} // close Survey Data check

} // close window on load
