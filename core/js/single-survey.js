	
window.onload = function() {

	var surveyResults = new Array(); 

	var answerReview = {
		template: '#answer-review', 
		data: function(){
			return {
				questions: [],
				totalQuestions: 0,				
			};
		},
		computed: {
			surveyResults: function(){
				return this.$parent.surveyResults;
			}							
		},
		mounted: function() {
	 		var self = this;
	 		jQuery.ajax({ 
				url: wpApiSettings.wpJson + 'pc-survey/v1/surveys?id=' + this.$parent.surveyId,
					success: function( res ){
						
						var arr = Object.keys(res).map(function(k) { return res[k] });

						arr.pop();

						self.questions = arr.filter( function(el){
							return el.type != 'page_break';
						});

						self.totalQuestions =  Object.keys(res).length-1;

	   			 	}
	 		});
	 	},
	};

	var surveyQuestion =  { 

		template: '#survey-question',
		
		data: function() {
			return { 
				counter: 0,	
				page: 0,				
				progress: 0,																		
				showButton: false,
			};				
		},

		computed:{

			questions: function(){
				return this.$parent.questions;
			},
			totalQuestions: function(){
				return this.$parent.totalQuestions;
			}
		},

	 	methods: {

	 		removeErrorClass:function(event){
	 			var el = event.srcElement
	 			if( el.classList.contains('input-error') ){
	 				el.classList.remove('input-error');
	 			}
	 			 
	 		},

	 		validateAnswers: function(){	 				

 				var validated = false;	
 				var x = 0; 			
 				var progress = 0;
 				var $toValidate = jQuery( '.survey-page input, .survey-page select, .survey-page textarea, .survey-page .heading, .survey-page .page-break, .survey-page .message' );
 					 				
 				if( $toValidate.length > 0){
 					$toValidate.each( function(){
 						if( !this.classList.contains('required') || this.value != ''){
 							x++;	
 							progress++;	 								 							
 							if( x == $toValidate.length ){
 								validated = true;
 							}
 						} else {
 							this.classList.add('input-error');
 						}
 					});
 				} else {
 					validated = true;
 				}

 				if( validated ){

 					this.progress += progress;					
 					return true;		 					
 				} else {
 					return false;
 				}	
 				 					 							 	
	 		}, 

	 		nextPage: function() {
	 			
	 			if ( this.validateAnswers() ){
	 				this.saveAnswers();			 				
	 				this.progress++;
	 				this.page++;
	 				if( this.page > this.$parent.lastPage ){
	 					this.$parent.surveyComplete = true;
	 				}			 				
	 			} 

	 			window.scrollTo( 0,0 );

	 		},

	 		prevPage: function(){
	 			this.removeProgress();
	 			this.page--;	 			
	 		},

  			removeProgress: function(){
  				var progress = 1
  				  				
  				var $toValidate = jQuery( '.survey-page input, .survey-page select, .survey-page textarea, .survey-page .heading, .survey-page .page-break, .survey-page .message' );
 				
 				$toValidate.each( function(){
					if( !this.classList.contains('required') || this.value != ''){								
						progress++;	 					
					} 
				});			

				this.progress = this.progress-progress;
  			},

	 		saveAnswers: function(){
	 			
	 			var $self = this;
	 			var $questions = jQuery('.survey-question');

	 			$questions.each( function() {

	 				var $key = jQuery(this).data('key');
	 				var $quan = null;
	 				
	 				if( $self.$parent.questions[$key].type == 'text' ){
	 					$quan = jQuery(this).find('input').val();	 		
	 				}

	 				if( $self.$parent.questions[$key].type == 'radio'  || 
	 					$self.$parent.questions[$key].type == 'rank'   ||  
	 					$self.$parent.questions[$key].type == 'yes_no' ||  
	 					$self.$parent.questions[$key].type == 'multiple'){

	 					$quan = jQuery(this).find('select').val();
	 				
	 				}
	
	 				if( $self.$parent.questions[$key].type == 'textarea' ){
						$quan = jQuery(this).find('textarea').val();
	 				}

	 				$self.$parent.questions[$key].answer = $quan;

	 			});
	 			
	 		}		 			 			 	
	 	}
	};

	var survey = new Vue({
	 	el: '#survey',		 	
	 	components: {
	 		'survey-question' : surveyQuestion,
	 		'answer-review' : answerReview,		 		
	 	},
	 	data: {
	 		questions: [],
	 		totalQuestions: [],
	 		surveyId: document.body.getAttribute( 'data-post-id' ),
	 		surveyComplete: false,
	 		showAnswersForReview: false,
	 		surveyResults: [],
	 		surveySubmitted: false,
	 		lastPage: 0,
	 	},
	 	mounted: function(){
			var self = this;
	 		jQuery.ajax({ 
				url: wpApiSettings.wpJson + 'pc-survey/v1/surveys?id=' + this.surveyId,
					success: function( res ){
						self.questions = res;
						self.totalQuestions =  Object.keys(res).length-1;
						self.lastPage = self.questions.last_page;
	   			 	}
	 		});
	 	},
	 	methods:{		 	
	 		submitSurvey: function(){

	 			var self = this;

	 			self.surveyResults = JSON.stringify( self.surveyResults ); 	 			 			
	 				
	 			jQuery.ajax({
	 				type: 'POST',
	 				url: wpApiSettings.wpJson + 'pc-survey/v1/submit',
	 				beforeSend: function( xhr){
	 					xhr.setRequestHeader( 'X-WP-Nonce', wpApiSettings.nonce );
	 				},	 					 				
	 				data: { 
	 					"title": Math.floor(Date.now() / 1000),
	 					"surveyID" : this.surveyId,	 					
	 					"content": this.questions,
	 				},
	 				success: function( response, textStatus, jqXHR ){
	 					self.surveySubmitted = true;
	 				}
	 			});
	 			

	 		},
	 		reviewAnswers: function(){
	 			this.showAnswersForReview = true;
	 		},
	 	}		 			 	
	});	

};