<style>

	#surveyData fieldset{
		clear: both;
		display: flex;
		flex-direction: column;
		margin: auto;					
	}

	#surveyData fieldset label{
		clear: both;
		float: left;
	}

	#surveyData fieldset select{
		float: left;
		width: 70%;					
		clear: both;
	}

	.chartWrapper{
		width: 33%;
	}

	.offScreen{
		display: none;
	}
	#loading{

		background-color: rgba(35,40,45,.76);

		position: fixed;
			bottom: 0;		
			left: 0;
			right: 0;
			top: 0;

		z-index: 2;
	}



	/* Print Styles */
	@media print{
		#adminmenumain, fieldset {
			display: none !important;
		}
		#wpcontent{
			margin: 1.25cm 3cm;
		}
		.result {
			page-break-inside: avoid;
		}
		.chartWrapper {
			width: 300px;
		}
		.chartWrapper canvas {
			height: auto !important;
			max-width: 100% !important; 
		}
		.chartWrapper.text, .chartWrapper.textarea{
			width: auto;
		}

	}

</style>

<h2>PC Survey App</h2>

<div id="surveyData">

	<div id="loading" v-if="loading">
		
	<svg width="200px" height="200px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-ellipsis" style="position: absolute;
	    background: none;left: 50%;top: 33%;">
	    <!--circle(cx="16",cy="50",r="10")-->
	    <circle cx="84" cy="50" r="0" fill="#feeb54">
	      <animate attributeName="r" values="10;0;0;0;0" keyTimes="0;0.25;0.5;0.75;1" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" calcMode="spline" dur="2s" repeatCount="indefinite" begin="0s"></animate>
	      <animate attributeName="cx" values="84;84;84;84;84" keyTimes="0;0.25;0.5;0.75;1" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" calcMode="spline" dur="2s" repeatCount="indefinite" begin="0s"></animate>
	    </circle>
	    <circle cx="16" cy="50" r="8.17521" fill="#dad69f">
	      <animate attributeName="r" values="0;10;10;10;0" keyTimes="0;0.25;0.5;0.75;1" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" calcMode="spline" dur="2s" repeatCount="indefinite" begin="-1s"></animate>
	      <animate attributeName="cx" values="16;16;50;84;84" keyTimes="0;0.25;0.5;0.75;1" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" calcMode="spline" dur="2s" repeatCount="indefinite" begin="-1s"></animate>
	    </circle>
	    <circle cx="84" cy="50" r="1.82479" fill="#2a3e83">
	      <animate attributeName="r" values="0;10;10;10;0" keyTimes="0;0.25;0.5;0.75;1" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" calcMode="spline" dur="2s" repeatCount="indefinite" begin="-0.5s"></animate>
	      <animate attributeName="cx" values="16;16;50;84;84" keyTimes="0;0.25;0.5;0.75;1" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" calcMode="spline" dur="2s" repeatCount="indefinite" begin="-0.5s"></animate>
	    </circle>
	    <circle cx="77.7957" cy="50" r="10" fill="#151e2f">
	      <animate attributeName="r" values="0;10;10;10;0" keyTimes="0;0.25;0.5;0.75;1" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" calcMode="spline" dur="2s" repeatCount="indefinite" begin="0s"></animate>
	      <animate attributeName="cx" values="16;16;50;84;84" keyTimes="0;0.25;0.5;0.75;1" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" calcMode="spline" dur="2s" repeatCount="indefinite" begin="0s"></animate>
	    </circle>
	    <circle cx="43.7957" cy="50" r="10" fill="#feeb54">
	      <animate attributeName="r" values="0;0;10;10;10" keyTimes="0;0.25;0.5;0.75;1" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" calcMode="spline" dur="2s" repeatCount="indefinite" begin="0s"></animate>
	      <animate attributeName="cx" values="16;16;16;50;84" keyTimes="0;0.25;0.5;0.75;1" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" calcMode="spline" dur="2s" repeatCount="indefinite" begin="0s"></animate>
	    </circle>
	  </svg>


	</div>


	
	<fieldset id="selectSurveyFieldset" class="selectSurvey">
		<label for="selectSurvey">Which survey would you like to view?</label>
		<select 
			name="selectSurvey" 
			id="selectSurvey" 
			class="formControl" 
			v-on:change="recalculateQuestions">

			<option disabled selected> --- </option>
			<option
				 v-for="( survey, key) in surveys" 
				 v-bind:value="key">
				 {{ survey }}
			 </option>
		</select>
	</fieldset>

	<fieldset id="currentPage" v-if="selectedSurvey">
		<label for="currentPageSelect">Page to View</label>
		<input type="number" min="1" :max="maxPage" placeholder="All" v-on:change="setCurrentPage" id="currentPageSelect">
	</fieldset>


	<fieldset id="filterFieldset" class="filter" v-if="filters" v-on:change="filterData" 
			style="right: 12.5%;position: absolute;background: white; padding: 1.25em;border: 1px solid black;">
		<label style="font-weight: bold;">{{filters.question}}</label>
		<select id="dataFilter">
			<option value="0">---</option>
			<option v-for="( value, key) in filters.answers" v-bind:value="key">{{value}}</option>
		</select>
		
		<span v-on:click="clearAllFilters()"
		  style="background: black;
			    color: white;
			    float: left;
			    clear: both;
			    padding: .5em;
			    margin: 1em 0;">    		
    		Clear filters
    	</span>

	</fieldset>

	<div v-for="( value, key ) in questions" class="result">
		<div v-if="visible( value.type, key, value.page )">			
			<h3 class="question"> {{ value.question }}</h3>
			<div :class="chartWrapperClass( value.type )" :id="key"></div>
			<hr>
		</div>
	</div>

</div>
