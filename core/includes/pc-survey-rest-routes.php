<?php
/*
 *
 * @param WP_REST_Request $request This function accepts a rest request to process data.
 */

function pc_survey_get_surveys( $request ) {
  
   	$results = array();

    if( $request['id'] ){

         $results = survey_get_questions( $request['id'] );

    } else {
        
        $args = array( 'post_type' => 'survey', 'posts_per_page' => -1);
        
        $surveys = get_posts( $args );

        foreach( $surveys as $survey ){

            $results[$survey->ID] = $survey->post_title;

        }
    }
 

    return rest_ensure_response( $results );
}

function pc_survey_get_results( $request ){

    $results = array();

    if( $request['id'] ){
        
        $filter = false;
        $filterValue = false;

        if( $request['filter'] && $request['filterValue'] ){                    
            $filter = $request['filter'];
            $filterValue = $request['filterValue'];
        } 

        $results = pc_survey_get_results_from_id( $request['id'] );
        $results = pc_survey_get_question_data( $results, $request['question'], $filter, $filterValue );      

    }

    return $results;

}


function pc_survey_submit_survey( $data ){

     $record_survey = wp_insert_post(
         array(
             'post_title' =>  $data['title'],
             'post_type' => 'survey_submission',
             'meta_input' =>  array( 
                'surveyResults' => $data['content'], 
                'surveyId' => $data['surveyID'] ),
             'post_status' => 'publish',             
         )
     );

    if( $record_survey ){
        return true;
    } else {
        return false;
    }

} 
  
 
/**
 * This function is where we register our routes 
 */

function register_pc_survey_submission_routes() {

    register_rest_route( 'pc-survey/v1', '/results', array(
        'methods'  => WP_REST_Server::READABLE, 
        'callback' => 'pc_survey_get_results',
    ) );


    register_rest_route( 'pc-survey/v1', '/surveys', array(
        'methods'  => WP_REST_Server::READABLE, 
        'callback' => 'pc_survey_get_surveys',
    ) );

    register_rest_route( 'pc-survey/v1', '/submit', array(
        'methods' => WP_REST_Server::EDITABLE,
        'callback' => 'pc_survey_submit_survey'
    ) );

}
 
add_action( 'rest_api_init', 'register_pc_survey_submission_routes' );