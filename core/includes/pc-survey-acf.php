<?php

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_591a0462c6896',
	'title' => 'Survey Builder',
	'fields' => array (
		array (
			'key' => 'field_591a0471b1101',
			'label' => 'Survey Content',
			'name' => 'survey_question',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'collapsed' => '',
			'min' => 1,
			'max' => 0,
			'layout' => 'row',
			'button_label' => '',
			'sub_fields' => array (
				array (
					'key' => 'field_591a049ab1103',
					'label' => 'Type',
					'name' => 'type',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'heading' => 'Heading',
						'message' => 'Message',
						'text' => 'Question - single line answer',
						'textarea' => 'Question	- paragraph answer',
						'radio' => 'Question - Select one answer from list',
						'multiple' => 'Question - select multiple answers from list',
						'rank' => 'Question - Rank from 1 - 5',
						'yes_no' => 'Question - Yes or No',
						'page_break' => 'Page Break',
					),
					'default_value' => array (
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array (
					'key' => 'field_591a0486b1102',
					'label' => 'Question',
					'name' => 'label',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_591a049ab1103',
								'operator' => '!=',
								'value' => 'message',
							),
							array (
								'field' => 'field_591a049ab1103',
								'operator' => '!=',
								'value' => 'heading',
							),
							array (
								'field' => 'field_591a049ab1103',
								'operator' => '!=',
								'value' => 'page_break',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array (
					'key' => 'field_591a0c5371d46',
					'label' => 'Required',
					'name' => 'required',
					'type' => 'checkbox',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_591a049ab1103',
								'operator' => '!=',
								'value' => 'message',
							),
							array (
								'field' => 'field_591a049ab1103',
								'operator' => '!=',
								'value' => 'heading',
							),
							array (
								'field' => 'field_591a049ab1103',
								'operator' => '!=',
								'value' => 'page_break',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'yes' => 'Yes',
					),
					'allow_custom' => 0,
					'save_custom' => 0,
					'default_value' => array (
						0 => 'false',
					),
					'layout' => 'vertical',
					'toggle' => 0,
					'return_format' => 'value',
				),
				array (
					'key' => 'field_591b06be8047d',
					'label' => 'Text',
					'name' => 'message',
					'type' => 'wysiwyg',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_591a049ab1103',
								'operator' => '==',
								'value' => 'message',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'tabs' => 'all',
					'toolbar' => 'full',
					'media_upload' => 1,
					'delay' => 0,
				),
				array (
					'key' => 'field_591b08821fc1e',
					'label' => 'Heading',
					'name' => 'heading',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_591a049ab1103',
								'operator' => '==',
								'value' => 'heading',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array (
					'key' => 'field_591a04f6b1104',
					'label' => 'Answers',
					'name' => 'answers',
					'type' => 'repeater',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_591a049ab1103',
								'operator' => '==',
								'value' => 'radio',
							),
						),
						array (
							array (
								'field' => 'field_591a049ab1103',
								'operator' => '==',
								'value' => 'multiple',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'collapsed' => '',
					'min' => 0,
					'max' => 0,
					'layout' => 'table',
					'button_label' => '',
					'sub_fields' => array (
						array (
							'key' => 'field_591a0510b1106',
							'label' => 'Answer',
							'name' => 'answer',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
						),
					),
				),
				array (
					'key' => 'field_59a436626d781',
					'label' => 'Is Filter',
					'name' => 'is_filter',
					'type' => 'true_false',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'default_value' => 0,
					'ui' => 0,
					'ui_on_text' => '',
					'ui_off_text' => '',
				),
			),
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'survey',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;