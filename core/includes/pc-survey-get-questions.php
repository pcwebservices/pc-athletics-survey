<?php

	function survey_get_questions( $id ){

		$id = intval($id);
		$questions = array();
		
		$questions['title'] = get_the_title( $id );

		$i = 0;
		$p = 0;

		$questions['last_page'] = 0;

		$survey = get_field('survey_question', $id);
		
		if( have_rows( 'survey_question', $id ) ){
			
		 	while( have_rows( 'survey_question', $id ) ) : the_row();
		
				$questions[$i]['type'] = get_sub_field('type');
				
				$filter = get_sub_field('is_filter');

				$questions[$i]['is_filter'] = $filter;
				
				if( $questions[$i]['type'] == 'page_break'){
					$p++;					
				} else {
					$questions[$i]['page'] = $p;
				}

				if( $questions[$i]['type'] != 'message' && $questions[$i]['type'] != 'page_break' && $questions[$i]['type'] != 'heading' ){
					$questions[$i]['question'] = get_sub_field('label'); 
					$required = get_sub_field('required');					
					 if( is_array($required) && $required[0] == 'yes' ){
					 	$questions[$i]['required'] = 'required';
					 }
					 


				}

				if( $questions[$i]['type'] == 'radio' || $questions[$i]['type'] == 'multiple'){

					$x = 0; 
					$answers = [];


					while( have_rows('answers')) : the_row();

						$answers['question-'.$x] = get_sub_field('answer'); 

						$x++;

					endwhile;

					$questions[$i]['answers'] = $answers;
			
				}

				if( $questions[$i]['type'] == 'message'){
					$questions[$i]['contents'] = get_sub_field('message');;
				}

				if( $questions[$i]['type'] == 'heading'){
					$questions[$i]['contents'] = get_sub_field('heading');
				}

		 		$i++;

		 		$questions['last_page'] = $p;

		 	endwhile;

		 }



		return $questions;

	}