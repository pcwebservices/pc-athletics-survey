<?php

	/*
		expects an id, which should be the post id of a survey 
		returns an array of quiz results	
	*/

	function pc_survey_get_results_from_id( $id ){

		$returned = array();

		// the survey results as stored as a string of json in the post meta

		//we have to get the right posts
		$args = array(
			'post_type' => 'survey_submission',
			'meta_key' => 'surveyId',
			'meta_value' => $id,
			'posts_per_page' => -1,
		);

		$survey_submissions = get_posts( $args );

		// and then loop through and grab the posts we need
		foreach( $survey_submissions as $key => $result){

            $meta = get_post_meta( $result->ID, 'surveyResults' );
            array_push( $returned, $meta );

        }

		return $returned;

	}

	/*

		expects an array of question results, and a question key
		returns a smaller array of all question results

	*/


	function pc_survey_get_question_data( $results, $question_id, $filter, $filterValue ){
		
		// an empty array to fill with questions		
		$arr = array();

		if( !$filter || !$filterValue){ 

			if( $question_id != null ){ 

				foreach ($results as $result){

					// there is some array nesting going on here so we have to grab the right array and log it
					$result = $result[0];

					// then we have to find the question that we're looking for
					foreach( $result as $key => $question ){						

						if( $key == $question_id ){
							
							array_push( $arr, $question );
						}  

					}

				}

			} else {

				foreach ($results as $result){

					// there is some array nesting going on here so we have to grab the right array and log it
					$result = $result[0];

					//gets all the questions
					foreach( $result as $key => $question ){					

						array_push( $arr, $question );				  

					}

				}



			}

			return $arr;	

		} //this prior loops handles the results if there was no filter presented

		foreach( $results as $result ){
			
			// there is some array nesting going on here so we have to grab the right array and log it
			$result = $result[0];

			if( $result[2]['answer'] == $result[2]['answers'][$filterValue] ){
			
				// then we have to find the question that we're looking for
				foreach( $result as $key => $question ){					

					if( $key == $question_id ){			
						//write_log( $result );
						array_push( $arr, $question );		

					}
				}
			}

		}

		if( count($arr) == 0 ){
			return false;
		}
		return $arr;	
	}
