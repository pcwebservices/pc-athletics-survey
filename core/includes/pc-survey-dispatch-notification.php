<?php
	function pc_survey_dispatch_notification($post_id, $passed, $attempt){

		// global $mgClient;
		// global $mailgun_domain;
		// $domain = $mailgun_domain;

		$studentEmail= get_post_meta( $post_id, 'submissionEmail', true );
		$message = pc_survey_email_template($post_id, $passed, $attempt);
		$headers = array('Content-Type: text/html; charset=UTF-8');
		$subject = 'Thank you for your submission';

		# Make the call to the client.
		// $result = $mgClient->sendMessage($domain, array(
		//     'from'    => 'PCSB <mailgun@YOUR_DOMAIN_NAME>',
		//     'to'      => $studentEmail,
		//     'subject' => 'Thank you for your submission',		   
		//     'html'    => $message,
		// ));		

		wp_mail( $studentEmail, $subject, $message, $headers);

	}



 	function pc_survey_email_template( $post_id, $passed, $attempt ){
	
 		$submissionJson = get_post_meta( $post_id, 'submissionJson', true);
		$finalScore = 0;
		
		$outputJson = json_decode( $submissionJson );

		foreach( $outputJson as $row){
			if ($row->correct == true) : $finalScore++; endif;
		}


		$msg = <<<EOT
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	<title>Thank you for your survey submission</title>

	<style type="text/css">table td {border-collapse: collapse;}</style>

</head>

<body>

	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;margin: 0;padding: 0;background-color: #555555;height: 100% !important;width: 100% !important;">


	<tr>

		<td align="center" valign="top" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;margin: 0;padding: 20px;border-top: 0;height: 100% !important;width: 100% !important;">

			<table border="0" cellpadding="0" cellspacing="0" width="600"  style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;border: 0;background:white;">


			<tr><td style="padding: 9px 18px;color: #EEEEEE;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: Helvetica;font-size: 11px;line-height: 125%;text-align: center;">We have received your survey submission. Thank you so much for taking our survey.</tr>


	<tr><td style='padding: 12px;text-align: center;'><img src='http://business.providence.edu/wp-content/uploads/2016/08/main-logo-2.png'>

	        </td></tr><tr><td style='padding: 20px;'><div style="padding:8px;background-color:#f2f2f2;font-size:.75EM;border-radius:10px;"><h2 style="font-family:Georgia, serif;text-align: center;">Thank you for taking our survey.</h2></div></td></tr>

EOT;

if( $passed == true){

$msg .= <<<EOT
	        <tr><td style="padding-top: 9px;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica;font-size: 15px;line-height: 150%;text-align: left;">Congratulations! You have passed the PCSB Academic Advising Assessment. You will now need to either bring a copy of your test results to your academic advisor or contact your advisor with a print out of your results to obtain your ALT PIN. </td></tr>

				<td style="padding: 20px; font-family: Georgia, serif;">

		<ul style="padding-top: 12px; padding-bottom: 12px; border-top: 2px solid #85754E; border-bottom: 2px solid #85654E;">

EOT;

} else{

	if( $attempt > 1){

	$msg .= <<<EOT

	    <tr><td style="padding-top: 9px;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica;font-size: 15px;line-height: 150%;text-align: left;">You have not passed the PCSB Academic Advising Assessment. As a result, you will now need to contact your academic advisor for further details on how to obtain your ALT PIN. </td></tr>

				<td style="padding: 20px; font-family: Georgia, serif;">

		<ul style="padding-top: 12px; padding-bottom: 12px; border-top: 2px solid #85754E; border-bottom: 2px solid #85654E;">

EOT;

	} else {

		$msg .= <<<EOT
			
        <tr><td style="padding-top: 9px;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #000000;font-family: Helvetica;font-size: 15px;line-height: 150%;text-align: left;">You have not passed the PCSB Academic Advising Assessment. You are eligible to take this exam one more time.  We recommend reviewing your academic guidebook before taking a second time.  As a reminder, you will need an 80% to pass this assessment and to receive your ALT PIN for registration.</td></tr>

				<td style="padding: 20px; font-family: Georgia, serif;">

		<ul style="padding-top: 12px; padding-bottom: 12px; border-top: 2px solid #85754E; border-bottom: 2px solid #85654E;">

EOT;

	}

}

$msg .= <<< EOT

			<li><strong>Your Name:</strong> 
EOT;

	$msg .= get_post_meta( $post_id, 'submissionName', true ) . "</li>";
	$msg .= "<li><strong>Your Email: </strong>" . get_post_meta( $post_id, 'submissionEmail', true ) ."</li>";
	$msg .=	"<li><strong>Your Major: </strong>" . get_post_meta( $post_id, 'submissionMajor', true ) . "</li>";
	$msg .= "<li><strong>Your Class Year: </strong>". get_post_meta( $post_id, 'submissionClassYear', true ) . "</li>";
	
	date_default_timezone_set('EST'); 
	
	$msg .=	"<li><strong>Time Submitted</strong>:" . date('F j Y g:i:s A e', $outputJson->timeStamp/1000 ) . "</li></ul>";
		
	$msg .= "<h2>Final Score: " . $finalScore * 10 . "%</h2>";

	$msg .= <<<EOT

	</div>

	<table border=1" class="widefat">
		<thead>
			<tr class="alternate">
				<th class="row-title" style="text-align: left;">Question</th>
				<th class="row-title" style="text-align: left;">Answer</th>
				<th class="row-title" style="text-align: left;">Correct</th>
			</tr>
		</thead>
EOT;


			foreach( $outputJson as $row ){
				if( $row->answered == true ){
					$msg .= '<tr valign="top">';

					$msg .= '<td style="font-size: 10px;">' . $row->questionTitle . '</td>';

					$msg .= '<td style="font-size: 10px;">' . $row->answerGiven . '</td>';
					
					if( $row->correct == true){	
						$msg .= '<td style="font-size: 10px;">Yes</td>';						
					} else {
						$msg .= '<td style="color: #dc3232; font-size: 10px;">No</td>';						
					}					
					
					$msg .= '</tr>';
				}
			}



			$msg .= <<<EOT
		
					</table>

				</td>

				</tr>

			</table>



			</td>

		</tr>

		</table>

	</body>

	</html>


EOT;

	return $msg;
}

