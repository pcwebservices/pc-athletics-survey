<?php

add_action('add_meta_boxes', 'pcsurvey_metaboxes');

function pcsurvey_metaboxes(){
	add_meta_box( 'pcsurvey_submission_metabox', 'Submission Results', 'pcsurvey_metaboxes_callback', 'survey_submission','normal','high');
}

function pcsurvey_metaboxes_callback( $post ){

	$survey_title = get_the_title( get_post_meta( $post->ID, 'surveyId', true ) );
	echo '<h2>Survey Taken: ' . $survey_title . '</h2>';
	
	echo '<hr><table border="1" style="width: 100%;"><thead><tr><th>Question</th><th>Answer</th></tr></thead><tbody>';

	$surveyResults = get_post_meta( $post->ID, 'surveyResults', true);	
	
	foreach( $surveyResults as $key => $result ){			
		if( $result["type"] == 'text' || $result["type"] == 'radio' ||  $result["type"] == 'textarea' ||  $result["type"] == 'yes_no' ||  $result["type"] == 'rank' ){
			echo '<tr><td style="width: 50%; padding: .75em;">' . $result['question'] . '</td><td style="width: 50%; padding: .75em;">' . $result['answer'] . '</td></tr>';		
		} elseif ( $result["type"] == 'multiple') {
			echo '<tr><td style="width: 50%; padding: .75em;">' . $result['question'] . '</td><td style="width: 50%; padding: .75em;">';
			foreach( $result['answer'] as $answer){
				echo $answer . '<br>';
			}
			echo '</td></tr>';			
		}
	}	

	echo '</tbody></table>';

	?>
<?php
	
}

