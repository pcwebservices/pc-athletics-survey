<?php

class PCSurveyApp {

	private $pc_survey_app_options;

	public function __construct() {
		add_action( 'admin_menu', array( $this, 'pc_survey_app_add_plugin_page' ) );
		add_action( 'admin_init', array( $this, 'pc_survey_app_page_init' ) );
	}

	public function pc_survey_app_add_plugin_page() {
		add_menu_page(
			'PC Survey App', // page_title
			'PC Survey App', // menu_title
			'manage_options', // capability
			'pc-survey-app', // menu_slug
			array( $this, 'pc_survey_app_create_admin_page' ), // function
			'dashicons-admin-generic', // icon_url
			71 // position
		);
	}

	public function pc_survey_app_create_admin_page() { ?>		

		<div class="wrap">
	
				<?php include 'pc-survey-chart-area.php'; ?>

				<!-- Begin Settings Section -->
<!-- 				<hr>

				<?php $this->pc_survey_app_options = get_option( 'pc_survey_app_option_name' ); ?>

				<?php settings_errors(); ?>

				<form method="post" action="options.php">
					<?php
						settings_fields( 'pc_survey_app_option_group' );
						do_settings_sections( 'pc-survey-app-admin' );
						submit_button();
					?>
				</form> -->


		</div>



	<?php }	

	public function pc_survey_app_page_init() {
		register_setting(
			'pc_survey_app_option_group', // option_group
			'pc_survey_app_option_name', // option_name
			array( $this, 'pc_survey_app_sanitize' ) // sanitize_callback
		);

		add_settings_section(
			'pc_survey_app_setting_section', // id
			'Settings', // title
			array( $this, 'pc_survey_app_section_info' ), // callback
			'pc-survey-app-admin' // page
		);

		add_settings_field(
			'mailgun_api_key_0', // id
			'Mailgun API Key', // title
			array( $this, 'mailgun_api_key_0_callback' ), // callback
			'pc-survey-app-admin', // page
			'pc_survey_app_setting_section' // section
		);

		add_settings_field(
			'mailgun_domain_1', // id
			'Mailgun Domain', // title
			array( $this, 'mailgun_domain_1_callback' ), // callback
			'pc-survey-app-admin', // page
			'pc_survey_app_setting_section' // section
		);
	}

	public function pc_survey_app_sanitize($input) {
		$sanitary_values = array();
		if ( isset( $input['mailgun_api_key_0'] ) ) {
			$sanitary_values['mailgun_api_key_0'] = sanitize_text_field( $input['mailgun_api_key_0'] );
		}

		if ( isset( $input['mailgun_domain_1'] ) ) {
			$sanitary_values['mailgun_domain_1'] = sanitize_text_field( $input['mailgun_domain_1'] );
		}

		return $sanitary_values;
	}

	public function pc_survey_app_section_info() {
		
	}

	public function mailgun_api_key_0_callback() {
		printf(
			'<input class="regular-text" type="text" name="pc_survey_app_option_name[mailgun_api_key_0]" id="mailgun_api_key_0" value="%s">',
			isset( $this->pc_survey_app_options['mailgun_api_key_0'] ) ? esc_attr( $this->pc_survey_app_options['mailgun_api_key_0']) : ''
		);
	}

	public function mailgun_domain_1_callback() {
		printf(
			'<input class="regular-text" type="text" name="pc_survey_app_option_name[mailgun_domain_1]" id="mailgun_domain_1" value="%s">',
			isset( $this->pc_survey_app_options['mailgun_domain_1'] ) ? esc_attr( $this->pc_survey_app_options['mailgun_domain_1']) : ''
		);
	}

}
if ( is_admin() )
	$pc_survey_app = new PCSurveyApp();

/* 
 * Retrieve this value with:
 * $pc_survey_app_options = get_option( 'pc_survey_app_option_name' ); // Array of All Options
 * $mailgun_api_key_0 = $pc_survey_app_options['mailgun_api_key_0']; // Mailgun API Key
 * $mailgun_domain_1 = $pc_survey_app_options['mailgun_domain_1']; // Mailgun Domain
 */
