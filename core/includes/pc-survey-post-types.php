<?php

// Register Custom Post Type
function pc_survey_app_add_post_types() {

	// Survey Custom Post Type
	$labels = array(
		'name'                  => 'Surveys',
		'singular_name'         => 'Survey',
		'menu_name'             => 'Surveys',
		'name_admin_bar'        => 'Survey',
		'archives'              => 'Survey Archives',
		'parent_item_colon'     => 'Parent Survey:',
		'all_items'             => 'All Surveys',
		'add_new_item'          => 'Add New Survey',
		'add_new'               => 'Add New Survey',
		'new_item'              => 'New Survey',
		'edit_item'             => 'Edit Survey',
		'update_item'           => 'Update Survey',
		'view_item'             => 'View Survey',
		'search_items'          => 'Search Survey',
		'not_found'             => 'Not found',
		'not_found_in_trash'    => 'Not found in Trash',
		'featured_image'        => 'Featured Image',
		'set_featured_image'    => 'Set featured image',
		'remove_featured_image' => 'Remove featured image',
		'use_featured_image'    => 'Use as featured image',
		'insert_into_item'      => 'Insert into item',
		'uploaded_to_this_item' => 'Uploaded to this item',
		'items_list'            => 'Items list',
		'items_list_navigation' => 'Items list navigation',
		'filter_items_list'     => 'Filter items list',
	);
	$args = array(
		'label'                 => 'Survey',
		'description'           => 'A survey of random questions',
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 41,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'menu_icon' 			=> 'dashicons-welcome-learn-more'

	);
	register_post_type( 'survey', $args );

	// Submission Custom Post Type
	$labels = array(
		'name'                  => _x( 'Survey Submissions', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Survey Submission', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Survey Submission', 'text_domain' ),
		'name_admin_bar'        => __( 'Survey Submission', 'text_domain' ),
		'archives'              => __( 'Survey Submission Archives', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Submission:', 'text_domain' ),
		'all_items'             => __( 'All Survey Submissions', 'text_domain' ),
		'add_new_item'          => __( 'Add New Survey Submission', 'text_domain' ),
		'add_new'               => __( 'Add New Survey Submission', 'text_domain' ),
		'new_item'              => __( 'New Survey Submission', 'text_domain' ),
		'edit_item'             => __( 'Edit Survey Submission', 'text_domain' ),
		'update_item'           => __( 'Update Survey Submission', 'text_domain' ),
		'view_item'             => __( 'View Survey Submission', 'text_domain' ),
		'search_items'          => __( 'Search Survey Submission', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),		
	);
	$args = array(
		'label'                 => __( 'Survey Submission', 'text_domain' ),
		'description'           => __( 'Survey submissions', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 41,
		'show_in_admin_bar'     => false,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'menu_icon' 			=> 'dashicons-welcome-learn-more',
		'show_in_rest'			=> 'true',
	);

	register_post_type( 'survey_submission', $args );
}

add_action( 'init', 'pc_survey_app_add_post_types', 0 );

